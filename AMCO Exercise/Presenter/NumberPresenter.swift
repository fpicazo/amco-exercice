//
//  NumberPresenter.swift
//  AMCO Exercise
//
//  Created by Javier Picazo Hernández on 4/29/19.
//  Copyright © 2019 JavsPH. All rights reserved.
//

import Foundation

class NumberPresenter {
   weak var delegate: NumberPresenterInterface!
    private let numberService: NumberService
    
    init(numberService: NumberService) {
        self.numberService = numberService
    }
    
    func validateData(number: Character){
        self.numberService.validateStringNumbers(number: number) { (response, textField) in
            if let numberMix = response{
                self.delegate.showMessage(message: numberMix, textTextField: textField, error: nil)
            }else{
                self.delegate.showMessage(message: nil, textTextField: textField, error: "Cadena de números invalida")
            }
           
        }
    }
    
    func removeData(){
        self.numberService.eraseCharacter { (textField) in
            self.delegate.showMessage(message: textField, textTextField: textField, error: nil)
        }
    }
}
