//
//  NumberService.swift
//  AMCO Exercise
//
//  Created by Javier Picazo Hernández on 4/29/19.
//  Copyright © 2019 JavsPH. All rights reserved.
//

import Foundation

class NumberService {
    var modelNumber: Number = Number(numberString: "")
    func validateStringNumbers(number: Character, callBack:(String?, String) -> Void) {
        if modelNumber.numberString.contains(number){
            callBack(nil, modelNumber.numberString)
        }else{
            if modelNumber.numberString.count > 0{
                let numberStringLast = Int(String(modelNumber.numberString.last!))
                let numberInt = Int(String(number))! - 1
                if numberInt == numberStringLast{
                    callBack(nil, modelNumber.numberString)
                }else{
                    modelNumber.numberString.append(number)
                    let mix = mixedNumber(numbers: modelNumber.numberString)
                    callBack(mix, modelNumber.numberString)
                }
            }else{
                modelNumber.numberString.append(number)
                callBack(modelNumber.numberString, modelNumber.numberString)
            }
        }
    }
    
   public func mixedNumber(numbers: String) -> String{
        var getNumbers = numbers
        var mix: String = ""
        while(!getNumbers.isEmpty){
            mix.append(getNumbers.removeFirst())
            if !getNumbers.isEmpty{
                mix.append(getNumbers.removeLast())
            }
        }
        return mix
    }
    
    func eraseCharacter(callBack:(String) -> Void){
        modelNumber.numberString.removeLast()
        let mix = mixedNumber(numbers: modelNumber.numberString)
        callBack(mix)
    }
}
