//
//  ViewController.swift
//  AMCO Exercise
//
//  Created by Javier Picazo Hernández on 4/29/19.
//  Copyright © 2019 JavsPH. All rights reserved.
//

import UIKit

class NumberViewController: UIViewController {
    //MARK: Var/Let
    private let presenter = NumberPresenter(numberService: NumberService())
    
    //MARK: IBOutles
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var mixedNumerLabel: UILabel!
    @IBOutlet weak var centerContraintTextField: NSLayoutConstraint!
    
    //MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyBoardObserver()
        self.hideLabel()
        self.initializeDelegates()
        self.numberTextField.keyboardType = .numberPad
    }


    //MARK: Initialize delegates
    fileprivate func initializeDelegates(){
        self.presenter.delegate = self
        self.numberTextField.delegate = self
    }
    
    fileprivate func showLabel(){
        self.messageLabel.isHidden = false
        self.mixedNumerLabel.isHidden = false
    }
    
    fileprivate func hideLabel(){
        self.messageLabel.isHidden = true
        self.mixedNumerLabel.isHidden = true
    }
}

//MARK: Extension
extension NumberViewController: NumberPresenterInterface{    
    func showMessage(message: String?, textTextField: String, error: String?){
        self.showLabel()
        self.numberTextField.text = textTextField
        if let error = error{
            self.messageLabel.text = error
            self.mixedNumerLabel.text = ""
            self.messageLabel.textColor = .red
        }else{
            self.messageLabel.text = "Cadena de números valida"
            self.mixedNumerLabel.text = message
            self.messageLabel.textColor = .white
        }
    }
}

extension NumberViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == ""{
            self.presenter.removeData()
        }else if string != " "{
            self.presenter.validateData(number: Character(string))
        }else if textField.text!.isEmpty{
            self.hideLabel()
        }
        return false
    }
    
}

extension NumberViewController{
    func addKeyBoardObserver(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.mainView.addGestureRecognizer(tapGesture)
        
        let defaultCenter = NotificationCenter.default
        
        defaultCenter.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        defaultCenter.addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillBeShown(_ notification: Notification){
        self.adjustingHeight(show: true, notification: notification)
    }
    
    @objc func keyboardWillBeHidden(_ notification: Notification){
        self.adjustingHeight(show: false, notification: notification)
    }
    
    func adjustingHeight(show:Bool, notification: Notification) {
        var userInfo = notification.userInfo!
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let changeInHeight = show ? CGFloat(-50.0) : CGFloat(50.0)
        UIView.animate(withDuration: animationDurarion) {
            self.centerContraintTextField.constant += changeInHeight
        }
    }
}


