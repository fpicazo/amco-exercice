//
//  NumberInterfaces.swift
//  AMCO Exercise
//
//  Created by Javier Picazo Hernández on 4/29/19.
//  Copyright © 2019 JavsPH. All rights reserved.
//

import Foundation

protocol NumberPresenterInterface: NSObjectProtocol{
    func showMessage(message: String?, textTextField: String, error: String?)
}
