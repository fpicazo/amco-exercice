//
//  AMCO_ExerciseTests.swift
//  AMCO ExerciseTests
//
//  Created by Javier Picazo Hernández on 4/28/19.
//  Copyright © 2019 JavsPH. All rights reserved.
//

import XCTest
@testable import AMCO_Exercise

class NumberServiceMock: NumberService{
    
}

class NumberPresenterTest: NSObject, NumberPresenterInterface{
    var setNumberMixed = false
    
    var numbersSet: String
    
    init(number: String) {
        numbersSet = number
    }
    
    func showMessage(message: String?, textTextField: String, error: String?) {
        if let _ = error{
            setNumberMixed = false
        }else{
            setNumberMixed = true
        }
    }
    
    
}

class AMCO_ExerciseTests: XCTestCase {
    var numberPresenter : NumberPresenter!
    var numberMockService : NumberService!
    var numberTestMistakeUpward: NumberPresenterTest!
    var numberTestMistakeRepeat: NumberPresenterTest!
    var numberTest: NumberPresenterTest!


    override func setUp() {
        super.setUp()
        numberMockService = NumberServiceMock()
        numberPresenter = NumberPresenter(numberService: numberMockService)
        numberTest = NumberPresenterTest(number: "6172839405")
        numberTestMistakeUpward = NumberPresenterTest(number: "380612")
        numberTestMistakeRepeat = NumberPresenterTest(number: "983616")
        
    }

    override func tearDown() {
        numberMockService = nil
        numberPresenter = nil
        numberTestMistakeUpward = nil
        numberTestMistakeRepeat = nil
        numberTest = nil
    }

    func testShouldSetNumberMistakeRepeat(){
        numberPresenter.delegate = numberTestMistakeRepeat
        for char in numberTestMistakeRepeat.numbersSet{
            numberPresenter.validateData(number: char)
            XCTAssert(numberTestMistakeRepeat.setNumberMixed)
        }
    }
    
    func testShouldSetNumberMistakeUpward(){
        numberPresenter.delegate = numberTestMistakeUpward
        for char in numberTestMistakeUpward.numbersSet{
            numberPresenter.validateData(number: char)
            XCTAssert(numberTestMistakeUpward.setNumberMixed)
        }
    }
    
    func testShoulSetNumber(){
        numberPresenter.delegate = numberTest
        for char in numberTest.numbersSet{
            numberPresenter.validateData(number: char)
            XCTAssert(numberTest.setNumberMixed)
        }
    }

}
